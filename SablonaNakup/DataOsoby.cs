﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    
    /// <summary>
    /// Všechna data osoby spolu se samotnou osobou.
    /// </summary>
    abstract class DataOsoba
    {
        /// <summary>
        /// Zajisti vytvoreni osoby a vygenerovani identifikatoru pro ni.
        /// </summary>
        /// <param name="poradi"></param>
        public DataOsoba(int poradi)
        {
            generujIdentifikator(poradi);
        }

        public Souradnice Souradnice;
        public int Penize;

        /// <summary>
        /// Vypise info o sobe na jednu radku.
        /// </summary>
        public abstract void VypisSe();

        /// <summary>
        /// Vraci jednopismenny identifikator pro zobrazeni v mape.
        /// </summary>
        public virtual char Identifikator
        {
            get
            {
                return identifikator;
            }
        }

        protected char identifikator;
        

        abstract protected void generujIdentifikator(int poradi);
    }

    class DataZlodej : DataOsoba
    {
        public Queue<char> OkradeniNakupujici;
        private bool zestarliOkradeni = false;
        public Zlodej osoba;

        public DataZlodej(int poradi, Zlodej zlodej) : base(poradi)
        {
            osoba = zlodej;
            OkradeniNakupujici = new Queue<char>();
        }

        protected override void generujIdentifikator(int poradi)
        {
            this.identifikator = (char)((int)'A' + (poradi));
        }

        /// <summary>
        /// Okradne nakupujiciho. Sebere mu 5 penez. Pokud nema dost, sebere mu vsechno.
        /// Pokud je nakupujici v seznamu nedavno okradenych, neokrade ho a vraci false. Jinak vraci true.
        /// </summary>
        /// <param name="nakupujici">Koho okradu.</param>
        /// <returns>Okradl jsem ho?</returns>
        public bool Okradni(DataNakupujici nakupujici)
        {
            if (OkradeniNakupujici.Contains(nakupujici.Identifikator))
            {
                return false; // nemuzu ho okrast
            }
            int castka = Math.Min(nakupujici.Penize, 5);
            nakupujici.Penize -= castka;
            Penize += castka;
            OkradeniNakupujici.Enqueue(nakupujici.Identifikator);
            zestarliOkradeni = true;
            return true;
        }

        /// <summary>
        /// Toto se vola jako informace, ze probehl tah a vsichni okradeni nakupujici tak maji zestarnout.
        /// Musi byt zavolana az na konci tahu (po pripadnem <see cref="Okradni(DataNakupujici)"/>).
        /// </summary>
        public void ProbehlTah()
        {
            if (!zestarliOkradeni) OkradeniNakupujici.Enqueue(' ');
            zestarliOkradeni = false;
            if(OkradeniNakupujici.Count > 10)
            {
                OkradeniNakupujici.Dequeue();
            }
        }

        public override void VypisSe()
        {
            Console.WriteLine("{0}: P{1,3}", Identifikator, Penize);
        }
    }

    class DataNakupujici : DataOsoba
    {
        public int Skore;
        public Nakupujici osoba;

        public DataNakupujici(int poradi, Nakupujici nakupujici) : base(poradi)
        {
            osoba = nakupujici;
            Penize = 100;
        }

        /// <summary>
        /// Nakoupi nabidku za cenu v argumentu. Pokud na ni nema dost penez, nenakoupi nic a vrati false. Jinak si odecte penize, pricte skore a vrati true;
        /// </summary>
        /// <param name="cena">Cena nabidky.</param>
        /// <returns>Jestli se nakup podaril.</returns>
        public bool Nakup(int cena)
        {
            if(Penize >= cena)
            {
                Penize -= cena;
                Skore++;
                return true;
            }
            return false;
        }

        public override void VypisSe()
        {
            Console.WriteLine("{0}: P{1,3} S{2,2}", Identifikator, Penize, Skore);
        }

        protected override void generujIdentifikator(int poradi)
        {
            this.identifikator = (char)((int)'a' + (poradi));
        }
    }
}
