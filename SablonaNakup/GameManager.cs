﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    class GameManager
    {

        protected Mapa mapa;

        protected int zbyva = 200;

        protected List<DataZlodej> Zlodeji;
        protected List<DataNakupujici> Nakupujici;

        protected List<Exception> NevypsaneVyjimky;

        // Seznamy obsahujici stejne zlodeje a nakupujici jako predchozi dva, ale pred kazdym tahem se zamichaji.
        private List<DataZlodej> ZlodejiPracovni;
        private List<DataNakupujici> NakupujiciPracovni;

        public GameManager()
        {
            NahodnyGeneratorMapy ng = new NahodnyGeneratorMapy();
            NevypsaneVyjimky = new List<Exception>();
            mapa = new Mapa(ng);
        }

        public GameManager(List<DataZlodej> zlodeji, List<DataNakupujici> nakupujici):this()
        {
            Zlodeji = zlodeji;
            Nakupujici = nakupujici;
            ZlodejiPracovni = new List<DataZlodej>(zlodeji);
            NakupujiciPracovni = new List<DataNakupujici>(nakupujici);
            Shuffle(ZlodejiPracovni);
            Shuffle(NakupujiciPracovni);
            mapa.Generuj(nakupujici, zlodeji);
        }

        public GameManager(List<Zlodej> zlodeji, List<Nakupujici> nakupujici):this()
        {
            Random r = new Random();
            Zlodeji = new List<DataZlodej>(zlodeji.Count);
            int i = 0;
            foreach (var zlodej in zlodeji)
            {
                zlodej.Nahoda = r;
                Zlodeji.Add(new DataZlodej(i++, zlodej));
            }
            i = 0;
            Nakupujici = new List<DataNakupujici>(nakupujici.Count);
            foreach (var n in nakupujici)
            {
                n.Nahoda = r;
                Nakupujici.Add(new DataNakupujici(i++, n));
            }
            ZlodejiPracovni = new List<DataZlodej>(Zlodeji);
            NakupujiciPracovni = new List<DataNakupujici>(Nakupujici);
            Shuffle(ZlodejiPracovni);
            Shuffle(NakupujiciPracovni);
            mapa.Generuj(Nakupujici, Zlodeji);
        }
        
        /// <summary>
        /// Vypise mapu a statistiku vsech osob.
        /// </summary>
        public void VypisStatistiku()
        {
            mapa.VypisSe();
            foreach (DataNakupujici n in Nakupujici)
            {
                n.VypisSe();
            }
            foreach (DataZlodej z in Zlodeji)
            {
                z.VypisSe();
            }
            foreach (Exception e in NevypsaneVyjimky)
            {
                Console.WriteLine(e.Message);
            }
            NevypsaneVyjimky.Clear();
        }

        /// <summary>
        /// Postara se o kroky vsech zucastnenych.
        /// </summary>
        public void Krok()
        {

            // Shuffle(ZlodejiPracovni);
            // Shuffle(NakupujiciPracovni); Domluvili jsme se, ze michat pred kazdym tahem nebudeme micha se misto toho jednou v konstruktoru.
            foreach (DataNakupujici nakupujici in NakupujiciPracovni)
            {
                try { 
                    Smer s = nakupujici.osoba.Krok(mapa.VratVysek(nakupujici.Souradnice), nakupujici.Penize);
                    Souradnice posunute = new Souradnice(nakupujici.Souradnice.X + s.X, nakupujici.Souradnice.Y + s.Y);
                    if (posunute.X >= 0 && posunute.X < mapa.Data.GetLength(0) && posunute.Y >= 0 && posunute.Y < mapa.Data.GetLength(1)) // nevyskocil jsem z mapy
                    {
                        if (mapa.JeVolno(posunute))
                        {
                            mapa.Data[nakupujici.Souradnice.X, nakupujici.Souradnice.Y] = ' ';
                            mapa.Data[posunute.X, posunute.Y] = nakupujici.Identifikator;
                            nakupujici.Souradnice = posunute;
                        }
                        int cena = mapa.CenaNabidky(posunute);
                        if (cena != -1)
                        {
                            if (nakupujici.Nakup(cena))
                            {
                                mapa.Data[nakupujici.Souradnice.X, nakupujici.Souradnice.Y] = ' ';
                                mapa.Data[posunute.X, posunute.Y] = nakupujici.Identifikator;
                                nakupujici.Souradnice = posunute;
                                mapa.GenerujNabidku();
                            }
                        }
                    }
                }catch(Exception e)
                {
                    NevypsaneVyjimky.Add(e);
                }
            }
            foreach (DataZlodej zlodej in ZlodejiPracovni)
            {
                try
                {
                    Smer s = zlodej.osoba.Krok(mapa.VratMapu(zlodej.OkradeniNakupujici), zlodej.Penize, zlodej.Souradnice);
                    Souradnice posunute = new Souradnice(zlodej.Souradnice.X + s.X, zlodej.Souradnice.Y + s.Y);
                    if (posunute.X >= 0 && posunute.X < mapa.Data.GetLength(0) && posunute.Y >= 0 && posunute.Y < mapa.Data.GetLength(1)) // nevyskocil jsem z mapy
                    {
                        if (mapa.JeVolno(posunute))
                        {
                            mapa.Data[zlodej.Souradnice.X, zlodej.Souradnice.Y] = ' ';
                            mapa.Data[posunute.X, posunute.Y] = zlodej.Identifikator;
                            zlodej.Souradnice = posunute;
                        }
                        if (mapa.JeNakupujici(posunute))
                        {
                            DataNakupujici okradeny = VratNakupujiciho(mapa.Data[posunute.X, posunute.Y]);
                            zlodej.Okradni(okradeny);
                        }
                    }
                    zlodej.ProbehlTah();
                }catch(Exception e)
                {
                    NevypsaneVyjimky.Add(e);
                }
            }
        }

        /// <summary>
        /// Vrati nakupujiciho v mape podle jeho id.
        /// </summary>
        /// <param name="id">Hledane id nakupujiciho.</param>
        /// <returns>Nakupujici nebo null, pokud takovy neexistuje.</returns>
        private DataNakupujici VratNakupujiciho(char id)
        {
            foreach (DataNakupujici nakupujici in Nakupujici)
            {
                if (nakupujici.Identifikator == id)
                    return nakupujici;
            }
            return null;
        }

        private static Random rng = new Random();

        private static void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}
